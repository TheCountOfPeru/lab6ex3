/** 
 * Started by M. Moussavi
 * March 2015
 * Completed by: STUDENT(S) NAME
 */

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadRecord {
    
	
    private ObjectInputStream input;
    
   
    /**
     *  opens an ObjectInputStream using a FileInputStream
     * @throws IOException 
     */
    private void readObjectsFromFile(String name) throws IOException
    {
        MusicRecord record;
        try
        {
            input = new ObjectInputStream(new FileInputStream( name ) );
        }
        catch ( IOException ioException )
        {
            System.err.println( "Error opening file." );
        }
        
        /* The following loop is supposed to use readObject method of of
         * ObjectInputSteam to read a MusicRecord object from a binary file that
         * contains several records.
         * Loop should terminate when an EOFException is thrown.
         */
        
        try
        {
            while ( true )
            {
            	
            	record = (MusicRecord)input.readObject();
            	System.out.println(record.getYear() + " " + record.getSongName() + " " 
            			+record.getSingerName() + " " +record.getPurchasePrice());
            	
            	
                // TO BE COMPLETED BY THE STUDENTS
                
           
            }   // END OF WHILE
        }
        catch ( EOFException x)
        {
        	input.close();
        }
        catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

    }           // END OF METHOD 
    
    
    public static void main(String [] args) throws IOException
    {
        ReadRecord d = new ReadRecord();
        d.readObjectsFromFile("C:/Users/Jonathan/repos/lab6ex3/allSongs.ser");
    }
}